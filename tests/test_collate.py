import unittest
from dnadna.utils.config import Config
from dnadna.collate import CollateTraining
from dnadna.snp_sample import SNPSample
import torch

fake_snps = [torch.zeros(3 + i, 3 + i) for i in range(5)]
fake_snps = [SNPSample(s[1:], s[0]) for s in fake_snps]
fake_params = [torch.rand(4, dtype=torch.float64) for _ in range(5)]
fake_batch = list(zip(range(5), [0] * 5, fake_snps, fake_params))


class TestCollate(unittest.TestCase):

    def test_collate_batch(self):
        """Test if the error is working properly"""
        config_0 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': False,
                'pad_left': False,
                'pad_bottom': False,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_0)
        self.assertRaises(ValueError, collate, fake_batch)

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
         0  0  0 -1 -1 -1 -1
         0  0  0 -1 -1 -1 -1
         0  0  0 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        config_1 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': False,
                'pad_bottom': False,
                'pad_top': True
            }
        })

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1]
        ])

        tensor4 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        collate = CollateTraining.from_config(config_1)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
         X  X  X -1 -1 -1 -1
         X  X  X -1 -1 -1 -1
         X  X  X -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_2 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': False,
                'pad_bottom': True,
                'pad_top': False
            }
        })

        collate = CollateTraining.from_config(config_2)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1  X  X  X
        -1 -1 -1 -1  X  X  X
        -1 -1 -1 -1  X  X  X
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, 0, 0, 0],
            [-1, -1, -1, -1, 0, 0, 0],
            [-1, -1, -1, -1, 0, 0, 0],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor4 = torch.tensor([
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ])

        config_3 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': False,
                'pad_left': True,
                'pad_bottom': True,
                'pad_top': False
            }
        })

        collate = CollateTraining.from_config(config_3)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1  X  X  X
        -1 -1 -1 -1  X  X  X
        -1 -1 -1 -1  X  X  X

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, 0, 0, 0],
            [-1, -1, -1, -1, 0, 0, 0],
            [-1, -1, -1, -1, 0, 0, 0]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0],
            [-1, -1, -1, 0, 0, 0, 0]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0],
            [-1, -1, 0, 0, 0, 0, 0]
        ])

        tensor4 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_4 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': False,
                'pad_left': True,
                'pad_bottom': False,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_4)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        Now we test the cases where two dual variables are set to true,
        i.e. pad_right = True and pad_left = True
        In this case, the input is in the middle of the array. If the dimension doesn't fit,
        the padding on the right and the padding on the bottom is privileged
        """

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1]
        ])

        tensor4 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_5 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': True,
                'pad_bottom': False,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_5)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
         X  X  X -1 -1 -1 -1
         X  X  X -1 -1 -1 -1
         X  X  X -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [0, 0, 0, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [0, 0, 0, 0, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [0, 0, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_6 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': False,
                'pad_bottom': True,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_6)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1 -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_7 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': True,
                'pad_bottom': True,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_7)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        Last batch of tests:
        We will test different case where the snp_dim or the
        indiv_dim is not set to max
        """

        """
        this format should be
        -1 -1 -1 -1 -1 -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1  X  X  X -1 -1
        -1 -1 -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        X  X  X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, 0, 0, 0, 0, -1, -1],
            [-1, -1, -1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1],
            [-1, 0, 0, 0, 0, 0, -1]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ])

        config_8 = Config({
            'collate': {
                'snp_dim': 5,
                'indiv_dim': 'max',
                'value_fill': -1,
                'pad_right': True,
                'pad_left': True,
                'pad_bottom': True,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_8)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1
        -1  X  X  X -1
        -1  X  X  X -1
        -1  X  X  X -1
        -1 -1 -1 -1 -1
        -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, 0, 0, 0, -1],
            [-1, 0, 0, 0, -1],
            [-1, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ])

        tensor2 = torch.tensor([
            [-1, -1, -1, -1, -1],
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ])

        tensor3 = torch.tensor([
            [-1, -1, -1, -1, -1],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [-1, -1, -1, -1, -1]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [-1, -1, -1, -1, -1]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ])

        config_9 = Config({
            'collate': {
                'snp_dim': 'max',
                'indiv_dim': 5,
                'value_fill': -1,
                'pad_right': True,
                'pad_left': True,
                'pad_bottom': True,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_9)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)

        """
        this format should be
        -1 -1 -1 -1 -1
        -1  X  X  X -1
        -1  X  X  X -1
        -1  X  X  X -1
        -1 -1 -1 -1 -1

        and then increase one by one for each input,
        to enventually reach

        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        X  X  X  X  X
        """

        tensor1 = torch.tensor([
            [-1, -1, -1, -1, -1],
            [-1, 0, 0, 0, -1],
            [-1, 0, 0, 0, -1],
            [-1, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1,]
        ])

        tensor2 = torch.tensor([
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [0, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1,]
        ])

        tensor3 = torch.tensor([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0]
        ])

        tensor4 = torch.tensor([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0]
        ])

        tensor5 = torch.tensor([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0]
        ])

        config_10 = Config({
            'collate': {
                'snp_dim': 5,
                'indiv_dim': 5,
                'value_fill': -1,
                'pad_right': True,
                'pad_left': True,
                'pad_bottom': True,
                'pad_top': True
            }
        })

        collate = CollateTraining.from_config(config_10)
        collated_batch = collate(fake_batch)

        assert torch.equal(collated_batch[1][0], tensor1)
        assert torch.equal(collated_batch[1][1], tensor2)
        assert torch.equal(collated_batch[1][2], tensor3)
        assert torch.equal(collated_batch[1][3], tensor4)
        assert torch.equal(collated_batch[1][4], tensor5)
