#!/bin/bash

user=$(whoami 2>/dev/null)

if [ $UID -ne 0 -a $UID -ne 1001 ]; then
    # The default UID for the dnadna user is 1001; if running under a different
    # (non-root) UID change its UID.
    eval $(fixuid -q)
    # If the user with $UID did not exist before fixuid, then whoami returned
    # empty before so we need to do this again:
    user=$(whoami 2>/dev/null)
fi


# Set up the micromamba environment
# source $MAMBA_ROOT_PREFIX/etc/profile.d/micromamba.sh
PS1="\h:\w\$ "
mamba activate dnadna

if [ -n "$DEV" -a "$DEV" != "0" ]; then
    # Create the user's micromamba environment, activate it
    if [ ! -d /home/${user}/.micromamba/envs/dnadna ]; then
        if conda env list | grep -q dnadna; then echo "The environnement dnadna already exists"; else mamba env create -n dnadna -f environment.yml ;fi
            mamba create -n dnadna --clone base
        pip install -e .
    fi

    # Ensure that the dnadna environment is auto-activated when starting a new
    # shell:
    echo "mamba activate dnadna" >> /home/${user}/.bashrc
fi

exec env PS1="$PS1" "$@"
