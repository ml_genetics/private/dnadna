import copy
import os.path as pth

from ..utils.cli import CommandWithPlugins, str_or_int


class TrainCommand(CommandWithPlugins):
    """
    Train a model on a simulation using a specified pre-processed training
    config.
    """

    logging = True
    # Override the default help message for --log-file
    logfile_format = '{model_name}_{run_name}_training.log'
    logging_args = copy.deepcopy(CommandWithPlugins.logging_args)
    logging_args['--log-file']['help'] += (
        f"by default logs to {logfile_format} in the run directory")

    @classmethod
    def create_argument_parser(cls, namespace=None, **kwargs):
        parser = super().create_argument_parser(namespace=namespace, **kwargs)
        parser.add_argument('-r', '--run-id', type=str_or_int,
                help='run ID to give to this training run (used to initialize '
                     'the output directory for the training run); may be '
                     'either a string or an integer; if unspecified, the '
                     'next unused integer ID is determined automatically')
        parser.add_argument('--overwrite',
                            help="overwrite run (otherwise, create a new run), "
                                 "if -r is not defined, overwrites last execution",
                            action="store_true")
        parser.add_argument('config',
            help='path to the training config file to use for this training '
                 'run')

        parser.add_argument('--resume', type=str, default=None,
            help='Continue the training with the model and optimizer weights.'
            ' `RESUME` is the path of the `.pth` file.')

        parser.add_argument('--transfer', type=str, default=None,
            help='path of the `.pth` file with optimizer initialization,'
                 ' and reset the epoch number to 0.')

        parser.add_argument('--freeze', action="store_true",
            help='False : all weights are trainable. '
            'True : all weights are freeze except the last layer')

        return parser

    @classmethod
    def run(cls, args):
        from ..training import ModelTrainer

        model_trainer = ModelTrainer.from_config_file(
                args.config, progress_bar=True)
        run_id, run_name, run_dir = model_trainer.get_run_info(args.run_id, args.overwrite)
        # Configure the default logfile if it was not specified by the user
        if args.log_file is None:
            # Make sure the run directory exists since it's where the log file
            # will be written
            model_trainer.ensure_run_dir(run_id, args.overwrite)
            filename = cls.logfile_format.format(
                    run_name=run_name, model_name=model_trainer.model_name)
            filename = pth.join(run_dir, filename)
            cls.configure_log_file(filename, args.log_level)

        assert not ((args.resume is not None) and (args.transfer is not None)
                    ), "You have to choose between '--resume' or '--transfer', not both."

        if args.resume is not None:
            (model_weight, init_opti) = (args.resume, False)
        elif args.transfer is not None:
            (model_weight, init_opti) = (args.transfer, True)
        else:
            (model_weight, init_opti) = (None, True)

        model_trainer.run_training(run_id=run_id, overwrite=args.overwrite,
                                   model_weight=model_weight, init_opti=init_opti,
                                   freeze=args.freeze)
