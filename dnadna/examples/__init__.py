"""
Example implementations of plugins, demonstrating how DNADNA can be customized.

Currently this includes just one example, the
`~dnadna.examples.one_event.OneEventSimulator` class, which provides an example
`~dnadna.simulator.Simulator` implementation.

Example modules
---------------

.. autosummary::
    :toctree: _autosummary

    dnadna.examples.one_event
"""
