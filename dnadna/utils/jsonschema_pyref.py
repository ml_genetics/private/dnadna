import json
import os.path as pth
import pkgutil
from importlib import import_module
from typing import List, Union
from urllib.parse import (ParseResult, urljoin as _urllib_urljoin,
                          urlparse as _urllib_urlparse)

try:
    import yaml  # type: ignore
except ImportError:
    yaml = None


__all__ = ['urljoin', 'urlparse']


_JSONTypes = Union[dict, list, str, int, float, bool, None]
_SchemaTypes = Union[dict, bool]


def urljoin(base: str, url: str, allow_fragments: bool = True) -> str:
    """
    Extends the stdlib's `urllib.parse.urljoin` to support the custom URL
    schemes implemented by this package.

    The join semantics for ``py-obj`` and ``py-pkgdata`` URLs differ from
    those of typical http-like URLs, as well as from each other.
    """
    parsedb = urlparse(base)
    if parsedb.scheme in _URL_SCHEME_JOINERS:
        return _URL_SCHEME_JOINERS[parsedb.scheme](base, url)

    return _urllib_urljoin(base, url, allow_fragments=allow_fragments)


def urlparse(url: str) -> ParseResult:
    """
    Extends the stdlib's `urllib.parse.urlparse` to support the custom URL
    schemes implemented by this package.
    """

    if ':' in url:
        scheme, _ = url.split(':', 1)
        if scheme in _URL_SCHEME_PARSERS:
            return _URL_SCHEME_PARSERS[scheme](url)

    return _urllib_urlparse(url)


def _is_dotted_identifier(s: str) -> bool:
    """
    Return True if string is one or more valid Python identifiers separated
    by ``.``.
    """

    s = s.strip()
    if not s:
        return False

    for ident in s.split('.'):
        if not ident.isidentifier():
            return False

    return True


def _resolve_url_py_obj(url: str) -> dict:
    parsed = _urlparse_py_obj(url)
    path = parsed.path
    modname = path
    objpath: List[str] = []
    module = None

    while module is None:
        try:
            module = import_module(modname)
        except ImportError:
            if '.' in path:
                # At least part of the dotted name is the name of the object
                # in the module, and not the module itself.
                modname, objname = modname.rsplit('.', 1)
                objpath.insert(0, objname)
            else:
                raise

    obj = module
    for idx, attr in enumerate(objpath):
        try:
            obj = getattr(obj, attr)
        except Exception:
            raise AttributeError(
                f"no attribute '{attr}' on "
                f"{'.'.join([modname] + objpath[:idx])}")

    if not isinstance(obj, dict):
        raise ValueError(
            f'path {path} does not resolve to a dict, which it must be in '
            f'order to be a JSON Schema')

    return obj


def _resolve_url_py_pkgdata(url: str) -> dict:
    parsed = _urlparse_py_pkgdata(url)
    path = parsed.path
    pkgname, filename = path.split('/', 1)

    _, ext = pth.splitext(filename)

    # TODO: Support more file types and a pluggable registry of loaders for
    # different types--as long as it can be loaded into a dict it can be
    # supported
    yaml_exts = {'.yaml', '.yml'}
    json_exts = {'.json'}
    supported_exts = yaml_exts | json_exts
    data = pkgutil.get_data(pkgname, filename)

    if data is None:
        raise RuntimeError(
            f'no resource named {filename} could be loaded from package '
            f'{pkgname}; see https://docs.python.org/3/library/pkgutil.html#pkgutil.get_data')

    if ext in yaml_exts:
        if yaml is None:
            raise RuntimeError(
                'reading schemas from YAML files requires PyYAML to be '
                'installed: https://pypi.org/project/PyYAML/')

        return yaml.safe_load(data)
    elif ext in json_exts:
        return json.loads(data)
    else:
        supported_exts = yaml_exts | json_exts
        raise ValueError(
            f'schemas are currently only loadable from files with the '
            f'following filename extensions: {sorted(supported_exts)}')


def _urljoin_py_obj(base: str, url: str) -> str:
    parsedb = _urlparse_py_obj(base)
    parsedu = urlparse(url)

    if parsedu.scheme:
        return url

    rel_depth = 0
    pathu = parsedu.path

    while pathu and pathu[0] == '.':
        rel_depth += 1
        pathu = pathu[1:]

    pathb = parsedb.path
    pathb_parts = pathb.split('.')

    if rel_depth > len(pathb_parts):
        raise ValueError(
            f'relative object path {url} outside the original path {base}')

    if rel_depth > 0:
        pathb = '.'.join(pathb_parts[:-rel_depth])

    new_url = 'py-obj:' + pathb

    if pathu:
        new_url += '.' + pathu

    if parsedu.fragment:
        new_url += '#' + parsedu.fragment

    return new_url


def _urljoin_py_pkgdata(base: str, url: str) -> str:
    # This follows similar semantics to normal URL joins w.r.t. the filename
    # path, so we actually pass this to urljoin but without the scheme (which
    # it doesn't recognize)
    parsedb = _urlparse_py_pkgdata(base)
    parsedu = urlparse(url)

    # If url is an absolute url with a scheme, what we do depends on the
    # scheme:
    # * If also py-pkgdata, check if the package names match; if so join just
    #   the filename, if not replace both the package name and the filename.
    #   This is a bit similar to stdlib urljoin() semantics for http URLs
    #
    # * If different scheme, also replace the base URL entirely.
    if parsedu.scheme == parsedb.scheme:
        # If pkgb == pkgu and filenameb == filenameu we take parsedu.path
        # But same if pkgb != pkgu or filenameb != filenameu,
        # So in all cases take the new path from url
        new_path = parsedu.path
    elif parsedu.scheme:
        # Some other scheme
        return url
    else:
        # A relative URL relative to the base py-pkgdata URL
        new_path = _urllib_urljoin(parsedb.path, parsedu.path)

    if parsedu.fragment:
        new_path += '#' + parsedu.fragment

    return 'py-pkgdata:' + new_path


def _urlparse_py_obj(url: str) -> ParseResult:
    """Parse URLs with the ``py-obj:`` scheme."""

    if not url.startswith('py-obj:'):
        raise ValueError(f'not a py-obj URL: {url}')

    _, rest = url.split(':', 1)
    if '#' in rest:
        path, fragment = rest.split('#', 1)
    else:
        path, fragment = rest, ''

    if not _is_dotted_identifier(path):
        fmt = 'py-obj:<identifier>[.<identifier>...][#<fragment>]'

        raise ValueError(
            f'py-obj URL path component {path} contains an invalid Python '
            f'indentifier; the correct format for a py-obj URL is: {fmt}')

    return ParseResult(scheme='py-obj', netloc='', path=path, params='',
                       query='', fragment=fragment)


def _urlparse_py_pkgdata(url: str) -> ParseResult:
    """Parse URLs with the ``py-pkgdata:`` scheme."""

    if not url.startswith('py-pkgdata:'):
        raise ValueError(f'not a py-pkgdata URL: {url}')

    _, rest = url.split(':', 1)
    if '#' in rest:
        path, fragment = rest.split('#', 1)
    else:
        path, fragment = rest, ''

    path = path.strip()
    fmt = 'py-pkgdata:<module>[.<module>...]/<path>[#<fragment>]'

    if '/' in path:
        module, filename = path.split('/', 1)
    else:
        module, filename = path, ''

    filename = filename.strip()

    if not filename:
        raise ValueError(
            f'py-pkgdata URL must contain a package-relative path to a file '
            f'contained in the package, starting with a "/"; the correct '
            f'format for a py-pkgdata URL is: {fmt}')

    if not _is_dotted_identifier(module):
        raise ValueError(
            f'py-obj URL module component {module} is not a valid Python '
            f'indentifier; the correct format for a py-obj URL is: {fmt}')

    return ParseResult(scheme='py-pkgdata', netloc='',
                       path=f'{module}/{filename}', params='', query='',
                       fragment=fragment)


URL_SCHEME_RESOLVERS = {
    'py-obj': _resolve_url_py_obj,
    'py-pkgdata': _resolve_url_py_pkgdata
}


_URL_SCHEME_JOINERS = {
    'py-obj': _urljoin_py_obj,
    'py-pkgdata': _urljoin_py_pkgdata
}


_URL_SCHEME_PARSERS = {
    'py-obj': _urlparse_py_obj,
    'py-pkgdata': _urlparse_py_pkgdata
}
