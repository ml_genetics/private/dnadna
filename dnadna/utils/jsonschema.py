"""Utilities for extending JSON-Schema validation of config files."""


import copy
import os
import os.path as pth
import pathlib
from collections.abc import Mapping
from datetime import datetime
from urllib.parse import urlparse
from urllib.request import url2pathname

import jsonschema
import referencing.jsonschema
from referencing.jsonschema import DRAFT7
from referencing import Registry, Resource
from .jsonschema_pyref import _resolve_url_py_obj
from jsonschema import (
    _keywords,
    _legacy_keywords,
)

from .serializers import DictSerializer


SCHEMA_SUPPORTED_VERSIONS = [
    jsonschema.Draft6Validator,
    jsonschema.Draft7Validator
]


SCHEMA_DIRS = [pathlib.Path(pth.dirname(pth.dirname(__file__))) / 'schemas']
"""Paths to built-in schemas."""


ERRORMSG_SCHEMA_BASE = {
    'oneOf': [
        {
            'type': 'string',
            'description':
                'a single error message to return for any error in validating '
                'the associated property',
            'minLength': 1,
            'format': 'errorMsg'
        }, {
            'type': 'object',
            'description':
                'a mapping from JSON-Schema validators (e.g. "type", '
                '"minLength", etc.) to the error message to use when a '
                'property fails that specific validator, e.g., a special '
                'message may be provided when a string fails to validate '
                'against "minLength" as opposed to "pattern"; the value '
                '"default" may be used as a fallback message to use for other '
                'validators not listed',
            'properties': {
                # Additional properties will be filled out by
                # make_config_schema depending on the metaschema we're
                # extending
                'default': {
                    'type': 'string',
                    'minLength': 1,
                    'format': 'errorMsg'
                }
            }
        }
    ]
}


def make_config_meta_schema(meta_schema):
    """
    Given an existing JSON-Schema meta-schema, it is extended by adding support
    for the ``"errorMsg"`` property, which provides custom error messages to
    return to users when a property in the schema fails validation.

    See `dnadna.utils.config.ConfigValidator.validate` for a worked example.
    """

    errormsg_schema = copy.deepcopy(ERRORMSG_SCHEMA_BASE)
    for prop in meta_schema.get('properties', {}):
        # Fill out the allowed properties for the object form of errorMsg
        # (the second choice in the oneOf array)
        errormsg_schema['oneOf'][1]['properties'][prop] = {
            'type': 'string',
            'minLength': 1,
            'format': 'errorMsg'
        }

    meta_schema = copy.deepcopy(meta_schema)
    meta_schema['properties']['errorMsg'] = errormsg_schema
    return meta_schema


class CustomValidationError(jsonschema.ValidationError):
    """
    Extends `jsonschema.ValidationError` for use with custom "errorMsg"
    errors.

    Because "errorMsg" supports some template variables in the error message,
    this stores the template and implements ``self.message`` as a property
    which renders the template.
    """

    @property
    def message(self):
        errormsg = self.__dict__['message']
        prop = '.'.join(str(p) for p in self.path)
        return errormsg.format(property=prop,
                               value=self.instance,
                               validator=self.validator,
                               validator_value=self.validator_value)

    @message.setter
    def message(self, value):
        self.__dict__['message'] = value


def make_config_validator(validator_cls=jsonschema.Draft7Validator,
                          get_path=None,
                          resolve_plugins=True,
                          resolve_defaults=True,
                          resolve_filenames=True,
                          posixify_filenames=False
                          ):
    """
    Creates a new `jsonschema.Draft7Validator` class fully overriding the built in of the
    ``validator_cls`` (`jsonschema.Draft7Validator` by default) which supports
    special functionality for DNADNA `.Config` objects, though it can be adapted
    to other types.

    The new validator has additional options for controlling how to resolve
    relative filenames in the schema and how to handle the ``default`` property
    in schemas.

    It also allows specifying a default format checker.

    See the `~dnadna.utils.config.ConfigValidator` class for more details and
    example usage.
    """
    # Note: We can't simply provide a format checker, or override the 'format'
    # property validator as they are only passed the value of the string
    # instance, and are not given an opportunity to modify its value.  In order
    # to *modify* the instance in-place we need to handle this at a higher
    # level where we have access to the full dict instance.
    validate_properties = validator_cls.VALIDATORS['properties']

    # From the DeprecationWarning: "Subclassing validator classes is not intended to "
    # "be part of their public API. A future version "
    # "will make doing so an error, as the behavior of "
    # "subclasses isn't guaranteed to stay the same "
    # "between releases of jsonschema. Instead, prefer "
    # "composition of validators, wrapping them in an object "
    # "owned entirely by the downstream library."

    # From now on, the attributs 'resolves' are to be passed to ConfigValidaror class
    # that wraps the new Draft7Validator
    def resolve_filename(instance, prop, subschema, get_path=None,
                         posixify=False):
        errors = []

        if subschema is True:
            return []

        # Handle format: filename
        format_ = subschema.get('format')
        if (subschema.get('type') == 'string' and
                format_ in ('filename', 'filename!')):
            filename = instance.get(prop)
            if isinstance(filename, str):
                if callable(get_path):
                    path = get_path(instance, prop)
                else:
                    path = '.'
                # If neither the filename nor its default are a string to
                # begin with we ignore it for now and deal with it later
                # when we do error checking
                filename = normpath(filename, relto=path, _posixify=posixify)

                if format_[-1] == '!' and not pth.exists(filename):
                    errors.append(
                        jsonschema.ValidationError(
                            f'property {prop} of {instance} must be an '
                            f'existing file, but {filename} does not exist'
                        )
                    )
                else:
                    instance[prop] = filename

        return errors

    def validate_config_properties(validator, properties, instance, schema):
        errors = []

        if resolve_filenames:
            posixify = posixify_filenames
            for prop, subschema in properties.items():
                errors += resolve_filename(instance, prop, subschema,
                                           get_path, posixify)

        # Now run the default properties validator
        errors = validate_properties(validator, properties, instance,
                                           schema)

        err_occurred = False
        orig_instance = copy.deepcopy(instance)

        # If there are no errors then the instance matches this schema or
        # sub-schema (in the case of a oneOf/allOf/anyOf); now we assign any
        # defaults so that defaults are only added from a schema that this
        # instance actually matches.  Then, if defaults were added we re-run
        # validation on the properties to ensure that the defaults are also
        # valid.
        if resolve_defaults:
            added_defaults = False
            for prop, subschema in properties.items():
                if subschema is True:
                    continue

                # Fill missing props with their defaults
                if (isinstance(instance, (dict, Mapping)) and
                        'default' in subschema and prop not in instance):
                    instance[prop] = subschema['default']
                    added_defaults = True

            if added_defaults:
                # We only need to re-validate if any defaults were actually
                # assigned
                errors = validate_properties(validator, properties, instance,
                                             schema)

        try:
            for error in errors:
                err_occurred = True
                yield error
        finally:
            # If an error occurred, clear any updates made to the instance
            # and replace it with its original values.
            # The general approach for dict to restore the same dict
            # object to a previous value
            if err_occurred:
                instance.clear()
                instance.update(orig_instance)

    # Create the format checker; filename and filename! are already handled
    # specially in validate_config_properties since it requires special support
    # for resolving relative filenames.  Other, simpler formats are checked
    # here
    format_checker = validator_cls.FORMAT_CHECKER

    @format_checker.checks('python-module', raises=(ImportError,))
    def check_python_module(instance):
        if isinstance(instance, str):
            __import__(instance)

        return True

    # supports special functionality for DNADNA `.Config` objects
    def is_config(checker, instance):
        return (
            isinstance(instance, (dict, Mapping))
        )
    type_checker = validator_cls.TYPE_CHECKER.redefine("object", is_config)

    # Creates a new `jsonschema.Draft7Validator` class fully overriding the built in of the
    # ``validator_cls`` (`jsonschema.Draft7Validator` by default)
    # This is to work around the issue in
    # https://github.com/python-jsonschema/jsonschema/issues/1197
    if validator_cls.__name__ == 'Draft7Validator':
        validator_cls = jsonschema.validators.create(
            meta_schema=make_config_meta_schema(validator_cls.META_SCHEMA),
            validators={
                "$dynamicRef": _keywords.dynamicRef,
                "$ref": _keywords.ref,
                "additionalProperties": _keywords.additionalProperties,
                "allOf": _keywords.allOf,
                "anyOf": _keywords.anyOf,
                "const": _keywords.const,
                "contains": _keywords.contains,
                "dependentRequired": _keywords.dependentRequired,
                "dependentSchemas": _keywords.dependentSchemas,
                "enum": _keywords.enum,
                "exclusiveMaximum": _keywords.exclusiveMaximum,
                "exclusiveMinimum": _keywords.exclusiveMinimum,
                "format": _keywords.format,
                "if": _keywords.if_,
                "items": _keywords.items,
                "maxItems": _keywords.maxItems,
                "maxLength": _keywords.maxLength,
                "maxProperties": _keywords.maxProperties,
                "maximum": _keywords.maximum,
                "minItems": _keywords.minItems,
                "minLength": _keywords.minLength,
                "minProperties": _keywords.minProperties,
                "minimum": _keywords.minimum,
                "multipleOf": _keywords.multipleOf,
                "not": _keywords.not_,
                "oneOf": _keywords.oneOf,
                "pattern": _keywords.pattern,
                "patternProperties": _keywords.patternProperties,
                "prefixItems": _keywords.prefixItems,
                "properties": validate_config_properties,
                "propertyNames": _keywords.propertyNames,
                "required": _keywords.required,
                "type": _keywords.type,
                "unevaluatedItems": _keywords.unevaluatedItems,
                "unevaluatedProperties": _keywords.unevaluatedProperties,
                "uniqueItems": _keywords.uniqueItems,
            },
            type_checker=type_checker,
            format_checker=format_checker,
            version="draft7",
            id_of=referencing.jsonschema.DRAFT7.id_of,
            applicable_validators=_legacy_keywords.ignore_ref_siblings,
        )
    elif validator_cls.__name__ == 'Draft6Validator':
        validator_cls = jsonschema.validators.create(
            meta_schema=make_config_meta_schema(validator_cls.META_SCHEMA),
            validators={
                "$ref": _keywords.ref,
                "additionalItems": _legacy_keywords.additionalItems,
                "additionalProperties": _keywords.additionalProperties,
                "allOf": _keywords.allOf,
                "anyOf": _keywords.anyOf,
                "const": _keywords.const,
                "contains": _legacy_keywords.contains_draft6_draft7,
                "dependencies": _legacy_keywords.dependencies_draft4_draft6_draft7,
                "enum": _keywords.enum,
                "exclusiveMaximum": _keywords.exclusiveMaximum,
                "exclusiveMinimum": _keywords.exclusiveMinimum,
                "format": _keywords.format,
                "items": _legacy_keywords.items_draft6_draft7_draft201909,
                "maxItems": _keywords.maxItems,
                "maxLength": _keywords.maxLength,
                "maxProperties": _keywords.maxProperties,
                "maximum": _keywords.maximum,
                "minItems": _keywords.minItems,
                "minLength": _keywords.minLength,
                "minProperties": _keywords.minProperties,
                "minimum": _keywords.minimum,
                "multipleOf": _keywords.multipleOf,
                "not": _keywords.not_,
                "oneOf": _keywords.oneOf,
                "pattern": _keywords.pattern,
                "patternProperties": _keywords.patternProperties,
                "properties": validate_config_properties,
                "propertyNames": _keywords.propertyNames,
                "required": _keywords.required,
                "type": _keywords.type,
                "uniqueItems": _keywords.uniqueItems,
            },
            type_checker=type_checker,
            format_checker=format_checker,
            version="draft6",
            id_of=referencing.jsonschema.DRAFT6.id_of,
            applicable_validators=_legacy_keywords.ignore_ref_siblings,
        )
    else:
        raise ValueError("Only supported Draft-06 (minimum) or Draft-07 JSON Schemas")

    return validator_cls


def _resolver_file_handler(uri):
    """
    Custom ``file://`` handler for RefResolver that can load schemas from YAML
    or JSON.

    Slightly hackish, but supported workaround to the larger issue discussed at
    https://github.com/Julian/jsonschema/issues/420
    """

    filename = url2pathname(urlparse(uri).path)
    return DictSerializer.load(filename)


def _retrieve(uri: str):
    """
    Dynamically read files off of the files and the custom ``py-obj`` object,
    which can be used to retrieve any schema which is not already pre-loaded in-memory.

    Parameters
    ----------
    uri : str
        A Uniform Resource Identifier (URI).

    Returns
    -------
    `referencing.Resource`
        A document (deserialized JSON) with a concrete interpretation under the spec DRAFT7.

    """
    base_url = f'file:///{SCHEMA_DIRS[0].as_posix()}/'

    scheme = urlparse(uri).scheme
    if not scheme:
        uri = base_url + uri
        scheme = urlparse(uri).scheme
    if scheme == 'file':
        contents = _resolver_file_handler(uri)
    elif scheme == 'py-obj':
        contents = _resolve_url_py_obj(uri)
    else:
        contents = {}

    if not contents.get('$id'):
        contents['$id'] = 'urn:unknown-dialect'

    return Resource.from_contents(contents, DRAFT7)


# a specific immutable set of in-memory schemas to be available in addition to the dynamic schemas
REGISTRY = Registry(retrieve=_retrieve)
# include all in-memory schemas in the directory SCHEMA_DIRS,
# making them available for use during validation.
for schema_dir in SCHEMA_DIRS:
    for curdir, dirs, files in os.walk(schema_dir):
        for filename in files:
            try:
                contents = DictSerializer.load(pth.join(curdir, filename))
                resource = Resource(contents=contents, specification=DRAFT7)
                REGISTRY = resource @ REGISTRY
            except NotImplementedError:
                # Not a format recognized by the dict serializer
                continue


def normpath(path, relto='.', _posixify=False):
    """
    Converts the given path to an absolute path.

    If it is a relative path, the path to which it is relative can be specified
    with the ``relto`` argument (if ``relto`` is a relative path then the final
    path is relative to the current working directory as with any
    `os.path.abspath` call).

    Examples
    --------

    >>> import os
    >>> import os.path as pth
    >>> from dnadna.utils.jsonschema import normpath
    >>> tmp = getfixture('tmpdir')  # pytest specific
    >>> _ = tmp.join('a').mkdir()
    >>> with tmp.join('a').as_cwd():
    ...     normpath('b', _posixify=True)
    ...     normpath('c', 'b', _posixify=True)
    ...     # should ignore relto given absolute path
    ...     normpath('/c', 'asdf', _posixify=True)
    '/.../a/b'
    '/.../a/b/c'
    '/c'
    """

    if not pth.isabs(path):
        path = pth.join(relto, path)

    path = pth.abspath(path)

    if _posixify:
        # "private" option used currently only for testing: makes a Windows
        # path beginning with a drive letter look like a POSIX path by dropping
        # the drive letter and switching the slash directions
        path = pth.splitdrive(path)[1].replace(os.sep, '/')

    return path


def timestamp(dt=None):
    """
    Returns a UTC datetime formatted according to the format expected by
    the JSON Schema "date-time" format.

    See
    https://json-schema.org/understanding-json-schema/reference/string.html#dates-and-times

    Examples
    --------

    >>> from dnadna.utils.jsonschema import timestamp
    >>> from datetime import datetime
    >>> dt = datetime(2021, 4, 8, 11, 11, 27)  # assumed UTC
    >>> timestamp(dt)
    '2021-04-08T11:11:27+00:00'
    """

    if dt is None:
        dt = datetime.utcnow()

    return dt.isoformat() + '+00:00'
