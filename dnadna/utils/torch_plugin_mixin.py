"""
Provides machinery for wrapping classes from PyTorch (or any other library,
though this is used primarily for wrapping PyTorch) in such a way that they are
recognized as implementing a DNADNA plugin interface.

See `dnadna.optim.Optimizer` for example of how this is used.
"""
import re
import warnings
from inspect import signature

from torch.optim.optimizer import required

from .. import DNADNAWarning
from .config import Config
from .decorators import classproperty
from .misc import dict_merge
from .plugins import gen_name_params_schema


def validate_lambda_expression(expr):
    try:
        pattern = r'^lambda\s+epoch\s*:\s*.*$'
        if not re.match(pattern, expr):
            raise ValueError("Invalid lambda function format")
        return eval(expr)
    except SyntaxError:
        raise ValueError("Invalid lambda function expression")
    except Exception as e:
        raise ValueError("Error while evaluating lambda function: " + str(e))


class TorchPluginMixin:
    """
    Provides a method `.from_torch` for generating a plugin class from a class
    in PyTorch (or any other library that implements classes with a compatible
    interface).

    For example, this is used by `dnadna.optim.Optimizer` to add PyTorch's
    Optimizer classes to a "pluggable" `~dnadna.optim.Optimizer` class as
    follows::

        import torch.optim
        from dnadna.utils.plugins import Pluggable
        from dnadna.utils.torch_plugin_mixin import TorchPluginMixin
        class Optimizer(torch.optim.Optimizer, TorchPluginMixin, Pluggable):
            ...

    This has 3 base classes: `torch.optim.Optimizer` to emphasize that plugins
    to this interface *should* subclass `torch.optim.Optimizer` (this may be
    optional so long as the plugins are duck-typeable as optimizers),
    ``TorchPluginMixin`` which provides the functionality for wrapping existing
    classes as plugins, and finally `dnadna.utils.plugins.Pluggable` which
    indicates that this class is "pluggable" (it is the parent class for a set
    of plugins).

    It's important to note that `TorchPluginMixin` can be used not only for optimizers
    but also for scheduler plugins, offering a flexible way to extend the functionality
    of the parent class with various PyTorch components.
    """

    param_aliases = {}
    """
    Maps plugin parameters read from the DNADNA config file to the
    corresponding keyword argument name of the wrapped class's initializer.

    E.g., the optimizers in `torch.optim` all take an argument called ``lr``
    for learning rate, but we also allow ``learning_rate``; see
    `~dnadna.optim.Optimizer.param_aliases`.
    """

    fixed_params = set()
    """
    A set of initializer argument names that are always passed a fixed value,
    and are not user-adjustable.  For example, for Optimizers the ``params``
    argument (confusingly also named "params").
    """

    schema = {}
    """Base schema for plugins."""

    @classproperty
    def param_aliases_inverse(cls):
        return {v: k for k, v in cls.param_aliases.items()}

    @classmethod
    def from_config(cls, config, *args, validate=True):
        """
        Initialize the plugin from that plugin's configuration format.

        Examples
        --------

        >>> from dnadna.optim import Optimizer
        >>> import torch.nn
        >>> mod = torch.nn.Linear(1, 1)  # a dummy module
        >>> optim = Optimizer.from_config({
        ...     'name': 'Adam',
        ...     'params': {
        ...         'learning_rate': 0.2,
        ...         'weight_decay': 0.01
        ...     }
        ... }, mod.parameters())
        ...
        >>> optim
        Adam (
        Parameter Group 0
            amsgrad: False
            betas: [0.9, 0.999]
            capturable: False
            differentiable: False
            eps: 1e-08
            foreach: None
            fused: None
            lr: 0.2
            maximize: False
            weight_decay: 0.01
        )
        """

        if not isinstance(config, Config):
            config = Config(config)

        if validate:
            config.validate(cls.get_schema())

        plugin_name = config['name']
        plugin_params = config['params'].dict()

        # Apply aliases to optimizer parameters
        for alias, param in cls.param_aliases.items():
            if alias in plugin_params:
                if param in plugin_params:
                    warnings.warn(
                        f'params for {plugin_name} contains both {alias} and '
                        f'{param} where {alias} is an alias for {param}; '
                        f'the value of {param} will be overridden',
                        DNADNAWarning)
                plugin_params[param] = plugin_params.pop(alias)

        # This case is for the scheduler using scheduler in parameters
        # We need to initialize recursively
        if 'schedulers' in plugin_params:
            scheduler_configs = plugin_params['schedulers']
            schedulers = []
            for scheduler_config in scheduler_configs:
                scheduler = cls.from_config(scheduler_config, *args, validate=validate)
                schedulers.append(scheduler)
            plugin_params['schedulers'] = schedulers

        # if 'lr_lambda' in plugin_params:
        #     if isinstance(plugin_params['lr_lambda'], str):
        #         plugin_params['lr_lambda'] =
        #         validate_lambda_expression(plugin_params['lr_lambda'])
        #     else:
        #         for i, lr_lambda in enumerate(plugin_params['lr_lambda']):
        #             plugin_params['lr_lambda'][i] = validate_lambda_expression(str(lr_lambda))
        return cls.get_plugin(plugin_name)(*args, **plugin_params)

    @classmethod
    def get_schema(cls):
        """
        Returns a schema pairing the ``plugin.name`` property with the valid
        ``plugin.params`` associated with that plugin (which may be very
        broad if the plugin subclass does not specify its ``.schema``).
        """

        # TODO: There needs to be a better way to do this; I am working on it
        # as part of my new plugin refactoring.
        if cls.pluggable is not cls:
            # This is a generic way of saying this a plugin to Optimizer and
            # not the Optimizer class itself.
            return cls.schema

        return gen_name_params_schema(cls, ['params'])

    @classmethod
    def from_torch(cls, torch_cls, schema_overrides={}):
        """
        Registers a class into the DNADNA plugin framework, as a plugin to the
        `~dnadna.utils.plugins.Pluggable` subclass that uses this mix-in.

        This automatically generates a config schema for each class, though the
        ``schema_overrides`` option allows overrides to the automatically
        generated one via `~dnadna.utils.misc.dict_merge`.

        .. warning::

            This does not yet support per-parameter optimizer options.
        """

        sig = signature(torch_cls)
        param_schemas = {}
        required_params = []

        for param in sig.parameters.values():
            if param.name in cls.fixed_params:
                # This is model parameters passed to the optimizer; it's not
                # declared as part of the schema, and is common to all
                # optimizers.
                continue

            if param.default is sig.empty:
                param_schemas[param.name] = {}
            else:
                param_schemas[param.name] = \
                    cls._schema_from_default(param.default)

            if param.default is required:
                # This is a special case for torch.optim.Optimizer, which
                # curiously is the only place this "required" placeholder is
                # used for required keyword arguments.  It seems like a
                # holdover from some earlier version.
                required_params.append(param.name)

        schema = {
            'properties': param_schemas,
        }

        if required_params:
            schema['required'] = required_params

        schema = dict_merge(cls.schema, schema, schema_overrides)

        # Add aliases to the schema
        # We don't make the aliases mutually exclusive; the expectation will be
        # the user will use one or the other but not both.  We could make them
        # strictly mutually-exclusive but this makes the schema much more
        # complex.
        for param, alias in cls.param_aliases_inverse.items():
            if param in schema['properties']:
                # Convert it into a pattern property supporting the alias
                # One downside to using a patternProperty is that it cannot
                # fill in defaults, but this is OK because we don't want it to
                # provide defaults for *both* the parameter and its alias.
                #
                # If lr/learning rate, for example, has a default value, it
                # will be provided by the __init__ of the Optimizer itself.
                # If it does not have a default value, and it was omitted from
                # the original config, then the config validation will fail
                # anyways due to the missing required parameter.
                param_schema = schema['properties'].pop(param)
                pattern = f'^{param}|{alias}$'
                pattern_props = schema.setdefault('patternProperties', {})
                pattern_props[pattern] = param_schema

            if param in required_params:
                # This is a tricky one to deal with...
                required_params.remove(param)
                all_of = schema.setdefault('allOf', [])
                all_of.append({
                    'oneOf': [
                        {'required': [param]},
                        {'required': [alias]}
                    ]
                })

        if required_params:
            schema['required'] = required_params

        return type(torch_cls.__name__, (torch_cls, cls),
                    {'schema': schema})

    @classmethod
    def _schema_from_default(cls, value, include_default=True):
        """
        Determines a basic schema for a keyword argument based on its default
        value.

        This currently works in a few specific, limited cases derived from the
        existing classes in `torch.optim`.  If it fails to guess, it will
        return just an empty schema.
        """

        param_schema = {}

        if isinstance(value, bool):
            # NOTE: Important for this to come before checking if it's an int,
            # since bools are also considered ints
            param_schema = {'type': 'boolean'}
        elif isinstance(value, (int, float)):
            # Numeric types are always treated as 'number' in the JSON
            # Schema sense: None of the current built-in optimizers have
            # arguments that strictly require ints; should one come along
            # that can be specified in a schema override.
            param_schema = {'type': 'number'}
            value = float(value)
        elif isinstance(value, str):
            param_schema = {'type': 'string'}
        elif isinstance(value, tuple):
            param_schema = {'type': 'array'}
            # param_schema['items'] = [cls._schema_from_default(v, False)
            #                          for v in value]
            # the items keyword to a single schema that will be used to
            # validate all of the items in the array.
            param_schema['items'] = cls._schema_from_default(value[0], False)
            value = list(value)
        elif value is None:
            param_schema = {'type': 'null'}

        if include_default and value is not required:
            param_schema['default'] = value

        return param_schema
