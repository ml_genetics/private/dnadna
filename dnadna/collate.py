import abc
from math import ceil, floor

import numpy as np
import torch

from .utils.config import Config
from .utils.plugins import Pluggable


class Collate(Pluggable, metaclass=abc.ABCMeta):
    """
    Parent Pluggable class to inherent from when the user want to write their
    own collate_batch function.

    The user must define a schema and can use the implementation of CollateTraining
    as an example of how the class should look. Then the main function to implement is
    _collate_batch, which will be called by default by the __call__ function.

    Here is an example using the class CollateTraining:

    from dnadna.collate import CollateTraining
    from dnadna.utils.config import Config
    collate = CollateTraining.from_config(config = Config({
         'collate': {
             'snp_dim': 'max',
             'indiv_dim': 'max',
             'value_fill': -1,
             'pad_right': True,
             'pad_left': False,
             'pad_bottom': True,
             'pad_top': False
         }
    }))
    collated_batch = collate(batch)
    """

    schema = {}

    def __init__(self, config):
        self.padding_params = config

    def __repr__(self):
        return str(self.padding_params)

    def __str__(self):
        return str(self.padding_params)

    def __call__(self, batch):
        return self._collate_batch(batch)

    @abc.abstractmethod
    def _collate_batch(self, batch):
        """
        Specifies how multiple scenario samples are collated into batches.

        Each batch element is a single element as returned by
        ``DNATrainingDataset.__getitem__``: ``(scenario_idx, replicate_idx,
        snp_sample, target)``.

        It is the main function to implement when heriting from Collate.
        See CollateTraining for an example of implementation
        """

    @classmethod
    def from_config(cls, config, validate=True):
        """
        Initialize a `Collate` from the batch configuration from a
        config file.

        Examples
        --------

        >>> from dnadna.collate import CollateTraining
        >>> from dnadna.utils.config import Config
        >>> collate = CollateTraining.from_config(config = Config({
        ...     'collate': {
        ...         'snp_dim': 'max',
        ...         'indiv_dim': 'max',
        ...         'value_fill': -1,
        ...         'pad_right': True,
        ...         'pad_left': False,
        ...         'pad_bottom': True,
        ...         'pad_top': False
        ...      }
        ... }))
        >>> collate
        Config({'snp_dim': 'max', 'indiv_dim': 'max', 'value_fill': -1, 'pad_right': True,
                'pad_left': False, 'pad_bottom': True, 'pad_top': False})
        """

        if not isinstance(config, Config):
            config = Config(config)

        if validate:
            config.validate(cls.get_schema())
        config = config['collate']

        return cls(config)

    @classmethod
    def get_schema(cls):
        """
        Returns the schema of the Collate implemented
        """

        return cls.schema


class CollateTraining(Collate):

    schema = {
        'properties': {
            'value_fill': {'type': 'number'},
            'snp_dim': {'type': ['string', 'number']},
            'indiv_dim': {'type': ['string', 'number']},
            'pad_right': {'type': 'boolean'},
            'pad_left': {'type': 'boolean'},
            'pad_bottom': {'type': 'boolean'},
            'pad_top': {'type': 'boolean'}
        }
    }

    def _collate_batch(self, batch):
        """
        Specifies how multiple scenario samples are collated into batches.

        Each batch element is a single element as returned by
        ``DNATrainingDataset.__getitem__``: ``(scenario_idx, replicate_idx,
        snp_sample, target)``.

        For input samples and targets are collated into batches "vertically",
        so that the size of the first dimension represents the number of items
        in a batch.

        Examples
        --------

        >>> import torch
        >>> from dnadna.collate import CollateTraining
        >>> from dnadna.utils.config import Config
        >>> from dnadna.snp_sample import SNPSample
        >>> fake_snps = [torch.rand(3, 3 + i) for i in range(5)]
        >>> fake_snps = [SNPSample(s[1:], s[0]) for s in fake_snps]
        >>> fake_params = [torch.rand(4, dtype=torch.float64) for _ in range(5)]
        >>> fake_batch = list(zip(range(5), [0] * 5, fake_snps, fake_params))
        >>> config = Config({
        ...     'collate': {
        ...         'snp_dim': 'max',
        ...         'indiv_dim': 'max',
        ...         'value_fill': -1,
        ...         'pad_right': True,
        ...         'pad_left': False,
        ...         'pad_bottom': True,
        ...         'pad_top': False
        ...      }
        ... })
        >>> collate_func = CollateTraining.from_config(config=config)
        >>> collated_batch = collate_func(fake_batch)
        >>> scenario_idxs, inputs, targets = collated_batch
        >>> bool((torch.arange(5) == scenario_idxs).all())
        True
        >>> inputs.shape  # last dim should be num SNPs in largest fake SNP
        torch.Size([5, 3, 7])
        >>> bool((inputs[0,:3,:3] == fake_snps[0].tensor).all())
        True
        >>> bool((inputs[0,3:,3:] == -1).all())
        True
        >>> bool((inputs[-1] == fake_snps[-1].tensor).all())
        True
        >>> targets.shape
        torch.Size([5, 4])
        >>> [bool((fake_params[bat].float() == targets[bat]).all())
        ...  for bat in range(targets.shape[0])]
        [True, True, True, True, True]
        """

        # filter any missing samples out of the batch
        batch = list(filter(lambda it: it[2] is not None, batch))

        # we have to consider the possibility of an empty batch (which could
        # happen if batch_size=1 and the sample was missing)
        if not batch:
            return None

        scen_idxs, _, samples, targets = zip(*batch)

        # add padding so that all input SNPs are the same size and shape
        # (though should all have the same number of rows, but may have
        # different number of SNPs (columns)
        # the value -1 is used for padded regions
        # TODO: Question: Must nets explicitly account for this padding, and if
        # so, how?  ReLU?
        # NOTE: This extra step of filling unevenly-sized matrices with -1
        # is unnecessary if we are using a dataset with uniform=True, so
        # there should be an option to skip this step entirely.  For now we
        # just check if all inputs are the same size
        inputs = [s.tensor for s in samples]

        pad_top = self.padding_params['pad_top']
        pad_bottom = self.padding_params['pad_bottom']
        pad_left = self.padding_params['pad_left']
        pad_right = self.padding_params['pad_right']

        if not (pad_top or pad_bottom) or not (pad_left or pad_right):
            raise ValueError("Invalid padding configuration. At least one horizontal"
                             "and one vertical value must be set to true.")

        snp_dim = self.padding_params['snp_dim']
        indiv_dim = self.padding_params['indiv_dim']
        filler = self.padding_params["value_fill"]
        max_indiv_dim = max(input.shape[1] for input in inputs)
        max_snp_dim = max(input.shape[0] for input in inputs)

        # Determine target dimensions based on input parameters
        target_indiv_dim = indiv_dim if isinstance(indiv_dim, int) else max_indiv_dim
        target_snp_dim = snp_dim if isinstance(snp_dim, int) else max_snp_dim

        # Initialize a tensor to store the padded inputs
        new_inputs = torch.zeros((len(inputs), target_snp_dim, target_indiv_dim))

        for i, input in enumerate(inputs):

            # This case is when "dim_snp" and "dim_indiv" are set to max
            if target_indiv_dim == max_indiv_dim and target_snp_dim == max_snp_dim:
                # Calculate padding dimensions based on boolean parameters
                if not (pad_bottom and pad_top) and not (pad_left and pad_right):
                    pad_dims = (
                        (0 if not pad_top else max_snp_dim - input.shape[0],
                         0 if not pad_bottom else max_snp_dim - input.shape[0]),

                        (0 if not pad_left else max_indiv_dim - input.shape[1],
                         0 if not pad_right else max_indiv_dim - input.shape[1])
                    )
                elif (pad_bottom and pad_top) and not (pad_left and pad_right):
                    pad_dims = (
                        # top then bottom
                        # If dimension doesn't divide by 2, the extra padding is done on the bottom
                        (floor((max_snp_dim - input.shape[0]) / 2),
                         ceil((max_snp_dim - input.shape[0]) / 2)),

                        (0 if not pad_left else max_indiv_dim - input.shape[1],
                         0 if not pad_right else max_indiv_dim - input.shape[1])
                    )
                elif not (pad_bottom and pad_top) and (pad_left and pad_right):
                    # left then right
                    # If dimension doesn't divide by 2, the extra padding is done on the right
                    pad_dims = (
                        (0 if not pad_top else max_snp_dim - input.shape[0],
                         0 if not pad_bottom else max_snp_dim - input.shape[0]),

                        (floor((max_indiv_dim - input.shape[0]) / 2),
                         ceil((max_indiv_dim - input.shape[0]) / 2)),
                    )
                else:
                    pad_dims = (
                        (floor((max_snp_dim - input.shape[0]) / 2),
                         ceil((max_snp_dim - input.shape[0]) / 2)),

                        (floor((max_indiv_dim - input.shape[0]) / 2),
                         ceil((max_indiv_dim - input.shape[0]) / 2)),
                    )

            # This case is when "dim_snp" and "dim_indiv" are set to integer values
            else:
                if not (pad_bottom and pad_top) and not (pad_left and pad_right):
                    pad_dims = (
                        (max(0, (0 if not pad_top else target_snp_dim - input.shape[0])),
                         max(0, 0 if not pad_bottom else target_snp_dim - input.shape[0])),

                        (max(0, (0 if not pad_left else target_indiv_dim - input.shape[1])),
                         max(0, 0 if not pad_right else target_indiv_dim - input.shape[1]))
                    )
                elif (pad_bottom and pad_top) and not (pad_left and pad_right):
                    pad_dims = (
                        # top then bottom
                        # If dimension doesn't divide by 2, the extra padding is done on the bottom
                        (max(0, (floor((target_snp_dim - input.shape[0]) / 2))),
                         max(0, ceil((target_snp_dim - input.shape[0]) / 2))),

                        (max(0, (0 if not pad_left else target_indiv_dim - input.shape[1])),
                         max(0, 0 if not pad_right else target_indiv_dim - input.shape[1]))
                    )
                elif not (pad_bottom and pad_top) and (pad_left and pad_right):
                    # left then right
                    # If dimension doesn't divide by 2, the extra padding is done on the right
                    pad_dims = (
                        (max(0, (0 if not pad_top else target_snp_dim - input.shape[0])),
                         max(0, 0 if not pad_bottom else target_snp_dim - input.shape[0])),

                        (max(0, (floor((target_indiv_dim - input.shape[0]) / 2))),
                         max(0, ceil((target_indiv_dim - input.shape[0]) / 2))),
                    )
                else:
                    pad_dims = (
                        (max(0, (floor((target_snp_dim - input.shape[0]) / 2))),
                         max(0, ceil((target_snp_dim - input.shape[0]) / 2))),

                        (max(0, (floor((target_indiv_dim - input.shape[0]) / 2))),
                         max(0, ceil((target_indiv_dim - input.shape[0]) / 2))),
                    )

            # Adjust dimensions if needed
            padded_input = np.pad(input, pad_dims, mode='constant', constant_values=filler)
            adjusted_input = padded_input[:target_snp_dim, :target_indiv_dim]

            new_inputs[i, :, :] = torch.as_tensor(adjusted_input).clone().detach()

        # Concatenate targets and ensure they are converted to single-precision
        # floats for passing to GPU devices
        # NOTE: targets can be all None if the Dataset was initialized without
        # scenario_params
        if targets[0] is not None:
            n_parameters = len(targets[0])
            targets = torch.cat(targets).reshape(-1, n_parameters).float()

        return [torch.tensor(scen_idxs, dtype=torch.long), new_inputs, targets]


class CollateInference(CollateTraining):

    def _collate_batch(self, batch):
        """
        Like `CollateTraining.collate_batch` but also returns the
        replicate indices and sample paths (e.g. filenames) which are used
        in the prediction output.
        """

        rep_idxs = [b[1] for b in batch]
        paths = [b[2].path for b in batch]
        out = super()._collate_batch(batch)
        return (out[:1] +
                [torch.tensor(rep_idxs, dtype=torch.long), paths] +
                out[1:])
